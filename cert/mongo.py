from pymongo import MongoClient
uri = "mongodb+srv://clusteraed.wawor.mongodb.net/?authSource=%24external&authMechanism=MONGODB-X509&retryWrites=true&w=majority"
client = MongoClient(uri,
                     tls=True,
                     tlsCertificateKeyFile='/home/pi/cert/X509-cert-4886629360802506344.pem')
from time import sleep
import time

class MongoPublisher():   

    def __init__(self):
        self.db = client['ClusterAed']
        # self.collection = db['state']

    def putState(self, state, time_param):

        collection = self.db['state']
        if (time_param !=0):
            data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'state': state, 'time_param': time_param }
        else:
            data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'state': state }
        print("mongo put: ",data)
        x = collection.insert_one(data)
        # print(x)

    def putMongoPH(self, PH):
        collection = self.db['ph_sensor']
        data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'ph': PH }
        print("mongo put: ",data)
        x = collection.insert_one(data)

    def putMongoHUM(self, HUM):
        collection = self.db['hum_sensor']
        data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'HUM': HUM }
        print("mongo put: ",data)
        x = collection.insert_one(data)
    
    def putMongoEC(self, EC):
        collection = self.db['ec_sensor']
        data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'EC': EC}
        print("mongo put: ",data)
        x = collection.insert_one(data)

    def putTempCollection(self, field, value):     
        collection = self.db['temp_sensor']
        data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), field: value }
        print("mongo put: ",data)
        x = collection.insert_one(data)  

    def putMongoTempInlet(self, temp):
        self.putTempCollection('inlet', temp)

    def putMongoExhaustTemp(self, temp):
        self.putTempCollection('exhaust_temp', temp)

    def putMongoTankTemp(self, temp):
        self.putTempCollection('main_tank', temp)

    def putMongoTempUpperTank(self, temp):
        self.putTempCollection('upper_tank', temp)

    def putMongoRoomLowTemp(self, temp):    
        self.putTempCollection('room_low', temp)
   
    def putMongoTempRoomMid(self, temp):    
        self.putTempCollection('room_mid', temp)

    def putMongoTopTemp(self, temp):    
        self.putTempCollection('room_top', temp) 
