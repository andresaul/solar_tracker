import module.sun
import module.light_sensor
import module.compass

import RPi.GPIO as GPIO
from time import sleep
from dateutil import tz
from datetime import datetime

from pymongo import MongoClient
uri = "mongodb+srv://cluster0.nrudrgv.mongodb.net/?authSource=%24external&authMechanism=MONGODB-X509&retryWrites=true&w=majority"
client = MongoClient(uri,
                     tls=True,
                     tlsCertificateKeyFile='/home/pi/cert/voka_cert.pem')
from time import sleep
import time


#Cluster0
#db = client['ClusterSolar']

#collection = db['solar_db']
#state='west'
#data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'state': state }
#print("mongo put: ",data)
#x = collection.insert_one(data)
#print(x)


light_threshold = 500
iterantion = 10

max_east = 17 #42
max_west = 47.2 #68

compass_mid = 40 # 62
mid_threshold = 2

def main():
    try:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(20, GPIO.OUT, initial=1)
        GPIO.setup(21, GPIO.OUT, initial=1)
#	a
        db = client['ClusterSolar']
        collection = db['solar_db']
        state='start'
        data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'state': state }
        print("mongo put: ",data)
        x = collection.insert_one(data)
        print(x)

        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        print(dt_string)

        if module.sun.is_dark():
            print("Its dark")
            collection = db['solar_db']
            state='is_dark'
            data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'state': state }
            print("mongo put: ",data)
            x = collection.insert_one(data)
            print(x)

            # print( abs(module.compass.magnet_x() - compass_mid) ) )
            if abs(module.compass.magnet_x() - compass_mid) > mid_threshold:
                move_center()
            quit()

        i = 0
        while True:
            if i > iterantion:
                print("Loop finished")
                break
            i = i + 1

            heading_angle = module.compass.heading()
            collection = db['solar_db']
            state='magnet'
            data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'state': state, 'value': heading_angle}
            print("mongo put: ",data)
            x = collection.insert_one(data)
            print(x)


            
            print(module.compass.magnet_x())

            if max_east >= module.compass.magnet_x():
                stop()
                print("Magnetometer max east")

                if module.light_sensor.more_light_west():
                    move_west(3)
                    print("east override")
                    sleep(3)
                break
            elif max_west <= module.compass.magnet_x():
                stop()
                print("Magnetometer max west")

                if module.light_sensor.more_light_east():
                    move_east(3)
                    sleep(3)
                break

            if module.light_sensor.diffrence()  < light_threshold:
                stop()
                print('good.spot->diffrence')
                print(module.light_sensor.diffrence())
                break
            elif module.light_sensor.more_light_west():
                move_west()
                # print('move->west')
            elif module.light_sensor.more_light_east():
                move_east()


    except:
        traceback.print_exc()
        print("An exception occurred")
    finally:
        stop()

def move_west(s = 1):
    db = client['ClusterSolar']
    collection = db['solar_db']
    state='move_west'
    data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'state': state }
    print("mongo put: ",data)
    x = collection.insert_one(data)
    print(x)

    GPIO.output(20, 0)
    GPIO.output(21, 1)
    print("Turn west")
    sleep(s)
    stop()

def move_east(s = 1):
    db = client['ClusterSolar']
    collection = db['solar_db']
    state='move_east'
    data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'state': state }
    print("mongo put: ",data)
    x = collection.insert_one(data)
    print(x)

    GPIO.output(20, 1)
    GPIO.output(21, 0)
    print("Turn east")
    sleep(s)
    stop()

def stop():
    GPIO.output(20, 1)
    GPIO.output(21, 1)
    #print("Stop")

def move_center():
    db = client['ClusterSolar']
    collection = db['solar_db']
    state='move_center'
    data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'state': state }
    print("mongo put: ",data)
    x = collection.insert_one(data)
    print(x)

    while True:
        x = module.compass.magnet_x()
        if abs(x - compass_mid) < mid_threshold:
            stop()
            print('center point')
            break
        elif x > compass_mid:
            move_east(2)
        elif x < compass_mid:
            move_west(2)

if __name__ == "__main__":
    main()

