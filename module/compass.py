import board 
import busio 
import adafruit_lsm303_accel 
import adafruit_lsm303dlh_mag 
import math

i2c = busio.I2C(board.SCL, board.SDA)
mag = adafruit_lsm303dlh_mag.LSM303DLH_Mag(i2c)
accel = adafruit_lsm303_accel.LSM303_Accel(i2c)

from pymongo import MongoClient
uri = "mongodb+srv://cluster0.nrudrgv.mongodb.net/?authSource=%24external&authMechanism=MONGODB-X509&retryWrites=true&w=majority"
client = MongoClient(uri,
                     tls=True,
                     tlsCertificateKeyFile='/home/pi/cert/voka_cert.pem')
from time import sleep
import time


def magnet_x():
    #print("Magnetometer (micro-Teslas)): X=%0.3f Y=%0.3f Z=%0.3f" % mag.magnetic)
    return mag.magnetic[0]

def magnet_y():
    # print("Magnetometer (micro-Teslas)): X=%0.3f Y=%0.3f Z=%0.3f" % mag.magnetic)
    return mag.magnetic[1]

def magnet_z():
    # print("Magnetometer (micro-Teslas)): X=%0.3f Y=%0.3f Z=%0.3f" % mag.magnetic)
    return mag.magnetic[2]



def heading():
    heading = 180 * math.atan2(magnet_x(), magnet_y()) / math.pi

    putMongo('mag_x', mag.magnetic[0])
    putMongo('mag_y', mag.magnetic[1])
    putMongo('mag_z', mag.magnetic[2])


    #print(heading)
    if heading < 0:
       heading += 360;
    return heading


def putMongo(state, value):


    db = client['ClusterSolar']
    collection = db['solar_db']
 #   state='light1'
    data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'state': state, 'value': value}
    print("mongo put: ",data)
    x = collection.insert_one(data)

