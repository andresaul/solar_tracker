import smbus
import board
import busio

from pymongo import MongoClient
uri = "mongodb+srv://cluster0.nrudrgv.mongodb.net/?authSource=%24external&authMechanism=MONGODB-X509&retryWrites=true&w=majority"
client = MongoClient(uri,
                     tls=True,
                     tlsCertificateKeyFile='/home/pi/cert/voka_cert.pem')
from time import sleep
import time

DEVICE = 0x23  # Default device I2C address
DEVICE2 = 0x5c  # Default device I2C address

POWER_DOWN = 0x00  # No active state
POWER_ON = 0x01  # Power on
RESET = 0x07  # Reset data register value

# Start measurement at 4lx resolution. Time typically 16ms.
CONTINUOUS_LOW_RES_MODE = 0x13
# Start measurement at 1lx resolution. Time typically 120ms
CONTINUOUS_HIGH_RES_MODE_1 = 0x10
# Start measurement at 0.5lx resolution. Time typically 120ms
CONTINUOUS_HIGH_RES_MODE_2 = 0x11
# Start measurement at 1lx resolution. Time typically 120ms
# Device is automatically set to Power Down after measurement.
ONE_TIME_HIGH_RES_MODE_1 = 0x20
# Start measurement at 0.5lx resolution. Time typically 120ms
# Device is automatically set to Power Down after measurement.
ONE_TIME_HIGH_RES_MODE_2 = 0x21
# Start measurement at 1lx resolution. Time typically 120ms
# Device is automatically set to Power Down after measurement.
ONE_TIME_LOW_RES_MODE = 0x23

bus = smbus.SMBus(1)  # Rev 2 Pi uses 1

def convertToNumber(data):
    # Simple function to convert 2 bytes of data
    # into a decimal number. Optional parameter 'decimals'
    # will round to specified number of decimal places.
    result = (data[1] + (256 * data[0])) / 1.2
    return (result)

def readLight(addr=DEVICE):
    # Read data from I2C interface
    data = bus.read_i2c_block_data(addr, ONE_TIME_HIGH_RES_MODE_1)
    a = convertToNumber(data)
    
    db = client['ClusterSolar']
    collection = db['solar_db']
    state='light1'
    data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'state': state, 'light1': a}
    print("mongo put: ",data)
    x = collection.insert_one(data)
    
    return a

def readLight2(addr=DEVICE2):
    # Read data from I2C interface
    data = bus.read_i2c_block_data(addr, ONE_TIME_HIGH_RES_MODE_1)
    a = convertToNumber(data)

    db = client['ClusterSolar']
    collection = db['solar_db']
    state='light2'
    data = {'time': time.strftime('%Y-%m-%d %H:%M:%S'), 'state': state, 'light2': a}
    print("mongo put: ",data)
    x = collection.insert_one(data)

    return a

def diffrence(): 
    west = readLight()  
    east = readLight2()
    diffrence = abs(west - east)
    return diffrence

def more_light_west():
    west = readLight()  
    east = readLight2()
    # print('more_west')
    # print(west)
    # print(east)
    return west > east

def more_light_east():
    west = readLight()  
    east = readLight2()
    # print('more_east')
    # print(west)
    # print(east)
    return west < east   
