import datetime
from suntime import Sun, SunTimeException
from dateutil import tz

def utc_to_local(utc_dt):
    return utc_dt.replace(tzinfo=timezone.utc).astimezone(tz=None)

latitude = 59.409241
longitude = 27.583130

def is_dark():
    to_zone = tz.gettz('Europe/Tallinn')
    now = datetime.datetime.now(to_zone)

    if (now > sunset()):
        print('dark')
        return True
    elif (now < sunrise()):
        print('dark2')
        return True

def sunset():
    sun = Sun(latitude, longitude)
    today_ss = sun.get_sunset_time()
    to_zone = tz.gettz('Europe/Tallinn')
    sunset = today_ss.astimezone(to_zone)
    return sunset

def sunset_hour():
    return sunset().strftime("%H")    
def sunset_minute():
    return sunset().strftime("%M")        

def sunrise():
    sun = Sun(latitude, longitude)
    today_sr = sun.get_sunrise_time()
    to_zone = tz.gettz('Europe/Tallinn')
    sunrise = today_sr.astimezone(to_zone)
    return sunrise

def sunrise_hour():
    return sunrise().strftime("%H")
def sunrise_minute():
    return sunrise().strftime("%M")
